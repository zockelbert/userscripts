// ==UserScript==
// @name        otr.datenkeller.net_GetDlUri
// @namespace   Violentmonkey Scripts
// @match       *://otr.datenkeller.net/?*
// @grant       none
// @version     1.08
// @author      Zockelbert
// @description Ermittelt Dateipfad aus OTR-Sendungsdownloadseiten
// @downloadURL https://codeberg.org/zockelbert/userscripts/raw/branch/main/otr.datenkeller.net_GetDlUri.user.js
// ==/UserScript==

'use strict';

/*
https://otr.datenkeller.net/
(OTR Mirror)

DK = datenkeller

Einstiegseite (Aufruf durch Suchmaschine)
https://otr.datenkeller.net/?file=S_W_A_T_20.05.20_20-15_rtlnitro_45_TVOON_DE.mpg.HQ.avi.otrkey&referer=otrkeyfinder&lang=de
window.location.search: ?file=S_W_A_T_20.05.20_20-15_rtlnitro_45_TVOON_DE.mpg.HQ.avi.otrkey&referer=otrkeyfinder&lang=de

Downloadseite
https://otr.datenkeller.net/?getFile=S_W_A_T_20.05.20_20-15_rtlnitro_45_TVOON_DE.mpg.HQ.avi.otrkey
window.location.search: ?getFile=S_W_A_T_20.05.20_20-15_rtlnitro_45_TVOON_DE.mpg.HQ.avi.otrkey
*/


(function ()
{
	//console.log(GM_info.script.name + " V" + GM_info.script.version);


	let DK_SearchURI = window.location.search;


	if (DK_SearchURI.startsWith("?file"))
	{
		const URIparams = new URLSearchParams(DK_SearchURI);
		window.location.href = "https://otr.datenkeller.net/?getFile=" + URIparams.get('file');
	}

	else if (DK_SearchURI.startsWith("?getFile"))
	{
		let theHTML = "";
		let theBody = document.body || document.getElementsByTagName('body')[0];

		if (document.querySelectorAll)
		{
			var MyTags = document.querySelectorAll('a');
			for (var i = 0; i < MyTags.length; i++)
			{
				if (MyTags[i].hasAttribute("onclick"))
				{
					var tagAttr = MyTags[i].getAttribute("onclick");
					if (tagAttr.substring(0, 10) === "startCount")
					{
						var tagAttrSplit = tagAttr.split("'");
						var DlUri = "https://" + tagAttrSplit[3] + "/" + tagAttrSplit[1] + "/" + tagAttrSplit[5];
						theHTML += "<p>Direktdownloadink:<br /><a href=\"" + DlUri + "\" style=\"color: black;\">" + DlUri + "</a>";
						theHTML += "<p>" + "Downloadadresse als Text:<br />" + DlUri + "</p>";
						theHTML += "<p style=\"color: lightgreen; background-color: #000000; text-align: center;\">Der Text unterhalb dieser Box dient nur zur Information.</p>";

						// div-Box am Seitenanfang einfügen
						var div1 = document.createElement("div");
						div1.id = 'OTRFilename';
						div1.style.display = 'block';
						div1.style.color = '#000000';
						div1.style.backgroundColor = '#99CC99';
						div1.style.border = '0.2em solid #000000';
						div1.style.padding = '0.5em';
						div1.style.fontSize = '120%';
						div1.style.fontWeight = 'bold';
						div1.innerHTML = theHTML;
						theBody.insertBefore(div1, theBody.firstChild);

						// Original Link-Text durchstreichen
						MyTags[i].setAttribute('style','text-decoration: line-through;');
					}
				}
			}
		}
	}
})();
