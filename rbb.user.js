// ==UserScript==
// @name        rbb
// @namespace   Violentmonkey Scripts
// @match       *://*.rbb-online.de/*
// @grant       GM_addStyle
// @version     1.04
// @author      Zockelbert
// @description 11.12.2020
// @downloadURL https://codeberg.org/zockelbert/userscripts/raw/branch/main/rbb.user.js
// ==/UserScript==

'use strict';

/*
rbb
https://www.rbb-online.de/

Ohne Verzeichnisangabe hinter der Domain wird umgeleitet auf:
https://www.rbb24.de/
##########
*/


(function ()
{
	// Testausgabe
	//console.info(GM_info.script.name + " V" + GM_info.script.version);

	let rbb_pathname = window.location.pathname;
	let MediathekSuche_Query = "station%3ARBB";
	let MediathekViewWeb_Query = "!RBB";

	// CSS anpassen
	let CSS = `
		a:any-link { color:black !important; }

		.ExtMediathek
		{
			font-weight: bold;
			margin: initial;
			color: black;
			background-color: red;
			text-transform: none !important;
		}
	`;
	// Appends and returns a <style> element with the specified CSS.
	let styleElement = GM_addStyle(CSS);


	if (rbb_pathname.startsWith("/abendschau/"))
	{
		//console.info("abendschau");
		MediathekSuche_Query = "station%3ARBB+Abendschau";
		MediathekViewWeb_Query = "!RBB%20%23Abendschau";
	}

	if (rbb_pathname.startsWith("/abendshow/"))
	{
		//console.info("abendshow");
		MediathekSuche_Query = "station%3ARBB+Abendshow";
		MediathekViewWeb_Query = "!RBB%20%23Abendshow";
	}

	if (rbb_pathname.startsWith("/rbb24/"))
	{
		//console.info("rbb24");
		MediathekSuche_Query = "station%3ARBB+rbb24";
		MediathekViewWeb_Query = "!RBB%20%23rbb24";
	}

	let ElementNav = document.getElementById("contentnav");
	if (ElementNav)
	{
		let ElementNavUL = ElementNav.querySelector('ul');
		if (ElementNavUL)
		{
			let NewElem = document.createElement('li');
		  NewElem.className = "ExtMediathek";
			NewElem.innerHTML = '<a href="https://mediatheksuche.de/vod?search=' + MediathekSuche_Query + '" title="MediathekSuche.de" target= "_top" class="ExtMediathek">MediathekSuche.de</a><br><a href="https://mediathekviewweb.de/#query=' + MediathekViewWeb_Query + '" title="MediathekViewWeb" target= "_top" class="ExtMediathek">MediathekViewWeb</a>';
			ElementNavUL.insertAdjacentElement('beforeend', NewElem);
		}
	}
})();
