// ==UserScript==
// @name		otrkeyfinder_Set_Favorite-Sites-Link
// @namespace	Violentmonkey Scripts
// @match		*://*.otrkeyfinder.com/*
// @grant		GM_addStyle
// @version		1.08
// @author		Zockelbert
// @description	OtrkeyFinder.com erweitern & Cookie-Zustimmungs-Box entfernen
// @downloadURL https://codeberg.org/zockelbert/userscripts/raw/branch/main/otrkeyfinder_Set_Favorite-Sites-Link.js
// ==/UserScript==

'use strict';

/*
OtrkeyFinder
https://www.otrkeyfinder.com/
##########
*/


(function ()
{
	//console.info(GM_info.script.name + " V" + GM_info.script.version);

	if (document.querySelectorAll)
	{
		function DeleteSearch ()
		{
			// Suchfeldinhalt löschen
			SearchNode.value = "";
			SearchNode.focus();
		}

		function ReloadPage ()
		{
			// Neuladen der Seite vom Server erzwingen.
			location.reload(true);
		}

		// otr.datenkeller.net-Link suchen & "Direktlink" einfügen
		var MyTags = document.querySelectorAll('a');
		for (let i = 0; i < MyTags.length; i++)
		{
			if (MyTags[i].innerText === "datenkeller.net")
			{
				var NewElem = document.createElement('a');
				var Filename = MyTags[i].href.match(/otrkey=.*\.otrkey/i).toString().split('=')[1];
				MyTags[i].title = "Hier NICHT klicken!";
				NewElem.href = "https://otr.datenkeller.net/?getFile=" + Filename;
				NewElem.target = '_top';
				NewElem.innerHTML = '&raquo; DOWNLOAD: Hier klicken &laquo;';
				NewElem.title = "Hier klicken!";
				NewElem.style='font-weight:bold; font-size:120%; text-decoration: underline overline;';
				MyTags[i].insertAdjacentElement('afterend', NewElem);
			}
		}

		// OTR-Lab-Link suchen & "Direktlink" einfügen
		for (let i = 0; i < MyTags.length; i++)
		{
			if (MyTags[i].innerText === "otr-lab.net")
			{
				NewElem = document.createElement('a');
				Filename = MyTags[i].href.match(/otrkey=.*\.otrkey/i).toString().split('=')[1];
				MyTags[i].title = "Hier NICHT klicken!";
				NewElem.href = "https://otr-lab.net/?file=" + Filename;
				NewElem.target = '_top';
				NewElem.innerHTML = '&raquo; DOWNLOAD: Hier klicken &laquo;';
				NewElem.title = "Hier klicken!";
				NewElem.style='font-weight:bold; font-size:120%; text-decoration: underline overline;';
				MyTags[i].insertAdjacentElement('afterend', NewElem);
			}
		}

	  // Suchfeldeingabe prüfen
		var SearchNode = document.getElementsByName('search')[0];
		var FormNode = SearchNode.parentNode;
		FormNode.addEventListener('submit', function (evt) {
			var Searchtext = SearchNode.value.trim();
			Searchtext = Searchtext.replace(/&[ ]?/g, "");
			Searchtext = Searchtext.replace(/[: ]/g, "_");
			// Veränderten Suchstring ins Suchfeld kopieren
			SearchNode.value = Searchtext;
			if (Searchtext.length < 3)
				if (! confirm("Der Suchbegriff muss mindestens 3 Zeichen enthalten.\nTrotzdem suchen?"))
					evt.preventDefault();
		});

		// Eingabefeldinhaltslöschknopf einfügen
		var DelButton = document.createElement("button");
		DelButton.type = 'button';
		DelButton.id = 'DelSearch';
		DelButton.title = "Eingabefeldinhaltslöschknopf";
		DelButton.innerText = "Eingabe löschen";
		DelButton.className = "button";
		DelButton.style = 'margin: 0.4em;';
		SearchNode.parentNode.insertBefore(DelButton, SearchNode.nextElementSibling.nextElementSibling);
		document.getElementById('DelSearch').addEventListener('click', DeleteSearch);

		// Neuladenknopf einfügen
		var ReloadButton = document.createElement("button");
		ReloadButton.type = 'button';
		ReloadButton.id = 'ReloadPage';
		ReloadButton.title = "Seiteneuladen erzwingen";
		ReloadButton.innerText = "Seite neu vom Server laden";
		ReloadButton.className = "button";
		ReloadButton.style = 'margin: 0.4em;';
		DelButton.insertAdjacentElement('afterend', ReloadButton);
		document.getElementById('ReloadPage').addEventListener('click', ReloadPage);

		// Cookie-Zustimmungs-Box entfernen
		// ID war mal "cookie-hint"
		let MyItemCookieConfirm = document.getElementById('cookie-compliance');
		if (MyItemCookieConfirm) MyItemCookieConfirm.remove();

		// Privatsphäre-Zustimmungs-Box entfernen
		let MyItemCmpConfirm = document.getElementById('qc-cmp2-container');
		if (MyItemCmpConfirm) MyItemCmpConfirm.remove();

		// Werbungs-Box entfernen
		let MyAdConfirm = document.getElementById('ad-bottom');
		if (MyAdConfirm) MyAdConfirm.remove();

		// Verhindern, dass Einträge beim Laden ohne aktiviertes JavaScript ausgeklappt sind
		let MyNoScriptTag = document.getElementsByTagName('noscript')[0];
		if (MyNoScriptTag) MyNoScriptTag.remove();

		// CSS anpassen
		let MyColors = "color:#000000; background-color:#99CC99;";
		let CSS = `
			body { ${MyColors} }
			.mirror-list { ${MyColors} width:50em; }
		`;

		// Appends and returns a <style> element with the specified CSS.
		let styleElement = GM_addStyle(CSS);

		// ContentDIV-Breite ändern
		document.getElementById("content").style.width = "90%";

		/*
		Störendes Element entfernen
		const div_bcast = document.querySelector('div.bcast-info');
		if (div_bcast) div_bcast.parentNode.removeChild(div_bcast);

		!!! Nicht entfernen!
		Enthält manchmal Link zur Mediathek
		*/
	}
})();
