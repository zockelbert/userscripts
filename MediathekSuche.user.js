// ==UserScript==
// @name        MediathekSuche
// @namespace   Violentmonkey Scripts
// @match       *://*.mediatheksuche.de/*
// @grant       GM_addStyle
// @version     1.15
// @author      Zockelbert
// @description Cookie-Zustimmungs-Box entfernen, Farben anpassen
// @downloadURL https://codeberg.org/zockelbert/userscripts/raw/branch/main/MediathekSuche.user.js
// ==/UserScript==

'use strict';

/*
MediathekSuche
https://www.mediatheksuche.de/
*/


(function ()
{
	//console.info(GM_info.script.name + " V" + GM_info.script.version);

	let $ID = function $ID(elem) {
		return document.getElementById(elem);
	};


	let $QS = function $QS(elem) {
		return document.querySelector(elem);
	};


	function DeleteSearch ()
	{
		// Suchfeldinhalt löschen
		let MyInputForm = $QS("input[name='search']");
		MyInputForm.value = "";
		MyInputForm.focus();
	}


	function ReloadPage ()
	{
		// Neuladen der Seite vom Server erzwingen.
		location.reload(true);
	}


	function DeleteSearch ()
	{
		// Suchfeldinhalt löschen
		let MyInputForm = $QS("input[name='search']");
		MyInputForm.value = "";
		MyInputForm.focus();
	}


	function ReloadPage ()
	{
		// Neuladen der Seite vom Server erzwingen.
		location.reload(true);
	}


	function Download(url)
	{
		let DLLinkUrl = url + '\n';
		const file = new File([DLLinkUrl], "download.download", {
			type: "text/plain",
		});

		let link = document.createElement('a');
		link.href = URL.createObjectURL(file)
		link.download = file.name
		document.body.appendChild(link)
		link.click()

		document.body.removeChild(link)
		window.URL.revokeObjectURL(file)
	}


	// CSS anpassen
	let CSS = `
		div.vod-details { display: block; padding: 15px; margin: 0 10px; background-color: #000 !important; background: #000 !important; }
		input[type="text"], input[type="search"] { background-color: LawnGreen; color: #000 !important; }
		input[type="text"]::placeholder, input[type="search"]::placeholder { color: #666; opacity: 1 !important; }
		body, header, footer, section, a.vod .title, a.vod .title > strong { color:#99CC99; background-color:#000000 !important; }
		a { color: lawngreen !important; }
		a.vod:hover, a.vod:focus { color:#000; background-color:DarkBlue; }
		a.vod:active { background-color:RoyalBlue; }
		#DelSearch, #ReloadPage, .btnSaveUrl {
			width: initial;
			height: 2.5em;
			padding: 0.1em;
			margin: 0.4em;
			color:#000000; background-color:Red;
			border-width: 0.4em;
			border-style: solid;
			border-color: FireBrick DarkRed DarkRed FireBrick;
		}
		#DelSearch:hover { border-color: DarkRed FireBrick FireBrick DarkRed; }
		#ReloadPage, .btnSaveUrl { background-color:LawnGreen; border-color: Lime LimeGreen LimeGreen Lime; margin: 0.2em; }
		#ReloadPage:hover, .btnSaveUrl:hover { border-color: LimeGreen Lime Lime LimeGreen; }

		#JumpToTop {
			display: flex;
			align-items: center;
			justify-content: center;
			margin: 2em;
			font-weight:bold;
		}

		div#header-content h1 { color: LimeGreen; }
	`;

	// Appends and returns a <style> element with the specified CSS.
	let styleElement = GM_addStyle(CSS);

	// Cookie-Zustimmungs-Box entfernen
	let CmpConfirm = $ID('cookie-compliance');
	if (CmpConfirm) CmpConfirm.remove();

	// Ersten "SubmitButton" suchen
	let SubmitButton = $QS('button[type="submit"]');
	if (SubmitButton)
	{
		// Text aus aria-label-Attribut in title-Attribut schreiben
		if (SubmitButton.hasAttribute("aria-label")) SubmitButton.title = SubmitButton.getAttribute("aria-label");

		let InputBtns = $QS('div.input-group');
		if (InputBtns)
		{
			// Eingabefeldinhaltslöschknopf einfügen
			let DelButton = document.createElement("button");
			DelButton.type = 'button';
			DelButton.id = 'DelSearch';
			DelButton.title = "Eingabefeldinhaltslöschknopf";
			DelButton.innerText = "Eingabe löschen";
			InputBtns.insertAdjacentElement('afterend', DelButton);
			$ID('DelSearch').addEventListener('click', DeleteSearch);

			// Neuladenknopf einfügen
			let ReloadButton = document.createElement("button");
			ReloadButton.type = 'button';
			ReloadButton.id = 'ReloadPage';
			ReloadButton.title = "Seiteneuladen erzwingen";
			ReloadButton.innerText = "Seite neu vom Server laden";
			DelButton.insertAdjacentElement('afterend', ReloadButton);
			$ID('ReloadPage').addEventListener('click', ReloadPage);
		}
	}

	// JumpToTop-Link in Fußzeile einfügen
	let Footer = document.getElementsByTagName("footer")[0];
	if (Footer)
	{
		let JumpToTopLink = document.createElement("a");
		JumpToTopLink.id = 'JumpToTop';
		JumpToTopLink.href = '#top';
		JumpToTopLink.innerText = "Zum Seitenanfang";
		Footer.insertAdjacentElement('beforeend', JumpToTopLink);
	}

	const vlinks = document.querySelectorAll("span.vlinks>div>div>a");
	if (vlinks)
	{
		vlinks.forEach((vlink) => {
			let Divider = document.createElement("span");
			Divider.innerHTML = ' | <button type="button" class="btnSaveUrl" data-uri="' + vlink.href + '" title="Nur die Linkadresse als Text herunterladen">SaveUrlOnly</button>';
			vlink.insertAdjacentElement('afterend', Divider);
		});

		document.addEventListener('click', function (event) {
			if (event.target.matches('.btnSaveUrl'))
			{
				Download(event.target.dataset.uri);
			}
		}, false);
	}
})();
